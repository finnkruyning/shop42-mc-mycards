$(document).ready(function() {

    /** feedback slide -------------------------------------------------------------------------------------- **/


    $('.js-feedback').each(function() {

        var elements = $(this).children('p');
        var nextElement = 0;
        var currentElement = null;
        var lengthElements = elements.length;

        showNextElement(nextElement);


        function showNextElement(next, current) {

            if (current !== undefined) {
                $(elements[current]).fadeOut();

            }
            $(elements[next]).fadeIn();

            currentElement = next;

            nextElement = next == lengthElements - 1 ? 0 : next + 1;
            setTimeout(function() {
                showNextElement(nextElement, currentElement)
            }, 6000);

        }

    });


    $('#body_zoeken').each(function(){
        $('#categories').hide();
        $('#pagecontent').css({'width':'100%'});
    });

});