
$(function () {
    var $body = $('body'),
        $win = $(window);
    function makeSticky() {
        var $scrollTop = $(document).scrollTop(),
            $stickyClass = "sticky";
        if ($scrollTop > 168) {
            if (!$body.hasClass($stickyClass)) $body.addClass($stickyClass);
        } else {
            if ($body.hasClass($stickyClass)) $body.removeClass($stickyClass);
        }
    }

    $win.on('scroll.makeSticky', function() {
        makeSticky();
    });

    var $header = $('#header');

    var $headerMenu = $header.find('.topbar-menu-right');
    $headerMenu.find('#navbar-basket').html($("#headerbasket #basket_count").html());
    if (jQuery("#headerlogout").length > 0) {
        logout = "<a href='/logout' title='Uitloggen' class='logout'><span>" + jQuery("#headerlogout span").html() + "</span></a>";
    }
    if (!$("#headerlogin").length) {
        $headerMenu.find('#navbar-login').remove();
    }

    if (!$("#headerlogout").length) {
        $headerMenu.find('#navbar-logout').remove();
    }

    // append visual content
    if ($header.find('#visual-content').length === 0) {
        $header.append('<div id="visual-content"></div>');
    }

    // stuff in mid
    var $mid = $('#mid');
    var $sidebar = $mid.find('#sidebar');
    var $pagecontent = $mid.find('#pagecontent');

    // set the menu's in sidebar. plaats in een pagina een <div data-sidebar="menu1|menu2|menut3" en in het menu de attr die je wilt laten zien <div id="menu1">
    var $dataSidebar = $pagecontent.find('[data-sidebar]');

    if ($dataSidebar.length) {
        $dataSidebar.data('sidebar').split("|").map(function (response) {
            if ($sidebar.find('#' + response).length) {
                $sidebar.find('#' + response).show();
            }
        });

    } else {
        $sidebar.find('.hide').show();
    }

    $('.feedback').innerfade({
        speed: 1000,
        timeout: 6000,
        type: 'random_start',
        containerheight: '120px'
    });

    //Sidebar footer inrichten
    if ($(window).width() >= 820) {
        if($('#sidebar-footer').length){
            $('#sidebar-footer').appendTo('#sidebar');
        }
    }

    if (jQuery(window).width() <= 820) {
        $('#leftnav-mobile').prependTo('body');
        if($('#loginFb').length){
            $('#login').after($('#loginFb'));
        }
    }

    //Design gallery functie
    if($('div.select_preview').length){
        $('div.select_preview>span').on('click', function(){
            var thisdex = $(this).index();
            $('#prevWrap>div').hide();
            $('#prevWrap>div:eq('+thisdex+')').show();
        })
    }

    var $pathname = window.location.pathname;
    $accordionbody = $('.accordion-body a');
    $('.accordion-body').find('a[href="'+$pathname+'"]').addClass('active');
    $accordionbody.each(function(){
        var $href = $(this).attr('href');
        if ($href == $pathname) {
            $(this).parents('.accordion-body').addClass('show').prev('.accordion-button').addClass('active');
        }
    });

    $.fn.scrumb = function($mainhead,$subhead){
        var $g = $(this).find('a[href="/account"]').parents('p');
        var $ht = '';
        if ($mainhead != '') $ht += '<h1 class="mainhead">' + $mainhead + '</h1>';
        if ($subhead != '') $ht += '<h2 class="subhead">' + $subhead + '</h2>';
        $g.after($ht);
        //$(this).find('a[href="/account"]').parents('p').hide();
    }

    var $btnBlock   = $("#body_account .button-block");
    $btnBlock.each(function(){
        var $url = $(this).find("> a"),
            $urlBlock = $url.attr("href");
        switch ($urlBlock) {
            case "account_orders":
                $(this).find(".button-block-title").after('<div class="button-block-text">Bekijk hier je bestelhistorie en<br>de status van je bestelling</div>');
                break;
            case "/eigen_collectie":
                $(this).find(".button-block-title").after('<div class="button-block-text">Bekijk hier je opgeslagen ontwerpen</div>');
                break;
            case "/account_address":
            //case "/account_adres_import":
                $(this).offsetParent().remove();
                break;
        }
    });


    if (jQuery('#body_account_settings').length >0) {
        var $b = jQuery('#pagecontent #pagebody');

        $ht = '<div id="crumbs"><a href="/">Home</a> &gt; <a href="/account">Account</a> &gt; Accountinstellingen</div><h1>Accountinstellingen</h1><div class="cols">';
        $ht += '<div class="md-4"><div class="button-block"><a href="/account_address" title="Persoonsgegevens"><span class="button-block-content"><span class="button-block-title">Persoonsgegevens</span></span></a></div></div>';
        $ht += '<div class="md-4"><div class="button-block"><a href="/account_password" title="Wachtwoord"><span class="button-block-content"><span class="button-block-title">Wachtwoord</span></span></a></div></div>';
        $ht += '<div class="md-4"><div class="button-block"><a href="/account_nieuwsbrief" title="Nieuwsbrief"><span class="button-block-content"><span class="button-block-title">Nieuwsbrief</span></span></a></div></div>';
        $ht += '</div><a id="step_back" href="/account"><i class="icon icon-arrow-left">&nbsp;</i> Terug naar mijn account</a>';

        $b.html($ht);
    }

    jQuery('#body_account_address #pagecontent').addClass('account-page').scrumb('Persoonsgegevens','');
    jQuery('#body_account_password #pagecontent').addClass('account-page').scrumb('Wachtwoord','');
    jQuery('#body_account_nieuwsbrief #pagecontent').addClass('account-page').scrumb('Nieuwsbrief','');
    jQuery('#body_account_orders #pagecontent').addClass('account-page').scrumb('Bestellingen','');
    jQuery('#body_account_adresboek #pagecontent').addClass('account-page').scrumb('Adresboek','');
    jQuery('#body_account_adres_import #pagecontent').addClass('account-page').scrumb('Adressen importeren','');
    jQuery('#body_account_settings #pagecontent').addClass('account-page').scrumb('Accountinstellingen','');

    if ($('#body_design').length) {
        $('.accordion.first').find('.accordion-button .icon').removeClass('icon-chevron-right').addClass('icon-chevron-down');
        $('.accordion.first').find('.accordion-body').addClass('show');

        var $text = '<div class="desUsp"><strong>Waarom MyCards.nl?</strong><ul><li><a href="/prijzen" title="Gratis verzenden">Gratis verzenden</a></li><li><a href="https://mycards.nl/papiersoorten" title="5 papiersoorten">5 papiersoorten</a></li><li><a href="https://mycards.nl/klantwaardering" title="99% aanbeveling!">99% aanbeveling!</a></li><li><a href="https://mycards.nl/prijzen" title="18 kleuren enveloppen">18 kleuren enveloppen</a></li><li><a href="#" title="Inclusief bedrukte achterkant">Inclusief bedrukte achterkant</a></li><li><a href="https://mycards.nl/bezorgen" title="Vóór 18:00 uur besteld, dezelfde dag verzonden">Vóór 18:00 uur besteld, dezelfde dag verzonden</a></li></ul></div>';
        $('#pagecontent .box-primary .box-footer').append($text);
    }

    if ($('#body_design .swiper-controls').length) {
        $('.swiper-controls').after('<div class="text-swiper">Klik voor een vergroting</div>');
    }

    $(window).load(function() {
        if ($('.Kleuren').length) {
            $('.Kleuren ul li').each(function(){
                var $value = $(this).find('input[type="checkbox"]').attr('value');
                $value = $value.replace(/-/g, ' ').replace(/(?:^|\s)\S/g, function (a) {
                    return a.toUpperCase();
                });
                $(this).find('label').attr('title',$value);
            });
        }
    });

    if($("#headerlogout").length > 0) {
        $(".logout-hide").hide();
    }
    if($("#headerlogin").length > 0) {
        $(".login-hide").hide();
    }

    if($('#body_design')) {
        $('span[class*="belgiansquare"]').addClass('square');
    }
});

function shuffle(o){
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};
