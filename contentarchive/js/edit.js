var matched, browser;

jQuery.uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
        /(msie) ([\w.]+)/.exec( ua ) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        [];

    return {
        browser: match[ 1 ] || "",
        version: match[ 2 ] || "0"
    };
};

matched = jQuery.uaMatch( navigator.userAgent );
browser = {};

if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if ( browser.chrome ) {
    browser.webkit = true;
} else if ( browser.webkit ) {
    browser.safari = true;
}

jQuery.browser = browser;
jQuery(function(){
jQuery('.lightbox').click(function(event){
		event.preventDefault();
		jQuery('body').append('<div id="bg-lightbox"></div>').show('slow');
		jQuery('body').append('<div id="lightbox"><script>jQuery(function(){jQuery("#bg-lightbox").click(function(){' +
			'jQuery(this).hide("slow");jQuery(this).remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});' +
			'jQuery("#lightbox .close").click(function(){jQuery("#bg-lightbox").hide("slow");jQuery("#bg-lightbox").remove();' +
			'jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});});</script><span class="close">Close</span>' +
			'<iframe src="/css/js/calculator/calculator.html"></iframe></div>').show('slow');
	});
	jQuery('#body_basket div#rebate div.button').addClass('tit-rebate');
	jQuery('#body_basket div#rebate div.tit-rebate').removeClass('button');
	jQuery('#body_basket div#rebate div.tit-rebate').attr('style','');
	jQuery('#body_basket div#rebate div.tit-rebate').text('Kortingscode:');
	jQuery('#body_basket div#rebate div.tit-rebate').next().addClass('cont-rebate');
	jQuery('#body_basket div#rebate input[type="submit"]').attr('value','Toevoegen');

	if (jQuery('#body_basket').length > 0) {
		jQuery('#basket_n td').each(function(){
			var text = jQuery(this).text();
			switch (text) {
				case 'Postzegels': jQuery(this).parent().hide();	break;
				case 'Cadeaus': jQuery(this).parent().hide();	break;
			}
		});
	}

	$('.social-right > li > a').hover(function () {
$(this).stop().animate({ 'left': 0 }, 200);
},function () {
$(this).stop().animate({ 'left': '117px' }, 200);
});
jQuery('#headeraccount').attr('target','_blank');

jQuery('#basket tbody').find('.hidden').eq(3).css('display','none');
jQuery('#basket tbody').find('.hidden').eq(4).css('display','none');

jQuery('#body_basket .email_block').parent().addClass('left-block');

jQuery("#body_address .address table tr td input#address_last_name").parent().addClass("col04");
jQuery("#body_address .address table tr td input#address_suffix").parent().addClass("col04");
jQuery("#body_address .address table tr td input#address_city").parent().addClass("col04");
jQuery("#body_addresses .address table tr td input#suffix").parent().addClass("col04");
jQuery("#body_address .address table tr td").removeAttr("style");
jQuery("#body_addresses .address table tr td").removeAttr("style");
jQuery("#body_basket #basket tr td").each(function(){
jQuery(this).find("div").addClass("style");
});

var hnavi = jQuery("#hnavi").html();
var headermenu = jQuery("#headermenu").html();
var top_head = '<div id="top-head">' + headermenu + hnavi + '</div>';
jQuery("#header").append(top_head);
if (jQuery('#canvasbackground').length >0) {
var bg = jQuery('#canvasbackground').attr('style');
if (bg.indexOf("double_landscape") != -1) {
jQuery('#body_edit #chooseside').css({'width':'328px','margin-left':'210px'});
}
}
if (jQuery(".tag_holder").length > 0) {
	jQuery(".tag_holder #tag .tag_group").each(function(){
		var $this = jQuery(this);
		$this.find("span").wrapAll("<div class='tag-inner'>");
	});
}

jQuery(window).load(function(){
	if (jQuery("#tag_dialog").length > 0) {
		jQuery("#tag_dialog #tag .tag_group b").click(function(){
			var $this1 = jQuery(this);
			if($this1.hasClass("opened")){
				$this1.next("div.tag-inner").slideUp();
				$this1.removeClass("opened");
			} else {
				jQuery("#tag_dialog #tag .tag_group b").removeClass("opened");
				jQuery("div.tag-inner").slideUp();
				$this1.next("div.tag-inner").slideDown();
				$this1.addClass("opened");
			}
		});
	}
});
});
