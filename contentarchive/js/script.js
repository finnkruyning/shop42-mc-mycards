var matched, browser;

jQuery.uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
        /(msie) ([\w.]+)/.exec( ua ) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        [];

    return {
        browser: match[ 1 ] || "",
        version: match[ 2 ] || "0"
    };
};

matched = jQuery.uaMatch( navigator.userAgent );
browser = {};

if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if ( browser.chrome ) {
    browser.webkit = true;
} else if ( browser.webkit ) {
    browser.safari = true;
}

jQuery.browser = browser;
function placeHolder(element){
	var standard_message = $(element).val();
	$(element).focus(
		function() {
			if ($(this).val() == standard_message)
				$(this).val("");
		}
	);
	$(element).blur(
		function() {
			if ($(this).val() == "")
				$(this).val(standard_message);
		}
	);
}

jQuery(function(){

	jQuery(window).load(function(){
		var win_wid = jQuery(window).width();
		if (win_wid < 1100) {
			var half = (1100 - win_wid)/2;
			jQuery('html, body').scrollLeft(half);
		}
	});
	jQuery('.lightbox').click(function(event){
		event.preventDefault();
		jQuery('body').append('<div id="bg-lightbox"></div>').show('slow');
		jQuery('body').append('<div id="lightbox"><script>jQuery(document).ready(function(){jQuery("#bg-lightbox").click(function(){jQuery(this).hide("slow");jQuery(this).remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});jQuery("#lightbox .close").click(function(){jQuery("#bg-lightbox").hide("slow");jQuery("#bg-lightbox").remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});});</script><span class="close">Close</span><iframe src="/css/js/calculator/calculator.html"></iframe></div>').show('slow');
	});

	jQuery('.biglink').click(function(){
		var url = jQuery(this).find('a').attr('href');
		window.location.assign(url);
	});

	jQuery('#megamenu li').each(function(){
		jQuery(this).find('.menu-content').parent().addClass('sub-menu');
	});

	$('.social-right > li > a').hover(function () {
		$(this).stop().animate({ 'left': 0 }, 200);
	},function () {
		$(this).stop().animate({ 'left': '117px' }, 200);
	});

	$('.feedback').innerfade({
	speed: 1000,
	timeout: 6000,
	type: 'random_start',
	containerheight: '120px'
	});

	jQuery('.hslider ul').bxSlider({
		slideWidth: 600,
		mode: 'fade',
		auto: true,
		pause: 6000,
		controls: false,
		pager: false
	});

	$("input[type='text']").each(function(index, element) {
		  placeHolder($(this));
	});

	jQuery("#effect01 li").each(function(){
		jQuery(this).find("p").append("<span></span>");
	});

	jQuery("#effect01 li").hover(function(){
		jQuery(this).find("a").css("color","#d19e33");
		jQuery(this).find("img").stop(true, true).animate({opacity: 0.8},500);
		//jQuery(this).find("p").find("span").stop(true, true).fadeIn();
	},function() {
		jQuery(this).find("a").css("color","#262626");
		jQuery(this).find("img").stop(true, true).animate({opacity: 1},500);
		//jQuery(this).find("p").find("span").stop(true, true).fadeOut();
	});

	jQuery("#card_previews a img").hover(function(){
		jQuery(this).stop(true, true).animate({opacity: 0.8},500);
	},function() {
		jQuery(this).stop(true, true).animate({opacity: 1},500);
	});

	jQuery('#body_account #pagecontent .button:eq(2)').hide();
	jQuery('#body_account #pagecontent .button:eq(3)').hide();


	jQuery("#form-style .btn-submit").click(function(){
		var arrName = new Array();
		var arrValue = new Array();
		var flag = true;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

		jQuery("#form-style input[type='text']").each(function(){
			var value = jQuery(this).val();
			var key = jQuery(this).attr('name');
			arrName.push(key);
			arrValue.push(value);
		});

		for (i=0;i<arrName.length;i++) {
			if (arrValue[i] == '') {
				jQuery("#form-style input[name='" + arrName[i] + "']").addClass('error');
				jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
				jQuery("#form-style input[name='" + arrName[i] + "']").after("<span class='message-error'>Verplicht veld</span>");

				flag = false;
			} else if (arrName[i] == 'email') {
				if( !emailReg.test( arrValue[i] ) ) {
					jQuery("#form-style input[name='" + arrName[i] + "']").addClass('error');
					jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
					jQuery("#form-style input[name='" + arrName[i] + "']").after("<span class='message-error'>Verplicht veld</span>");

					flag = false;
				}
			} else {
				jQuery("#form-style input[name='" + arrName[i] + "']").removeClass('error');
				jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
				flag = true;
			}
		}

		var textarea = jQuery("#form-style textarea").val();
		if (textarea == '') {
			jQuery("#form-style textarea").addClass('error');
			jQuery("#form-style textarea").next().remove();
			jQuery("#form-style textarea").after("<span class='message-error'>Verplicht veld</span>");
			flag = false;
		} else {
			jQuery("#form-style textarea").removeClass('error');
			jQuery("#form-style textarea").next().remove();
			flag = true;
		}

		if (!flag) {
			return false;
		}
	});



	jQuery("#navInfo").hover(function(){
		jQuery(this).addClass('current');
		jQuery('ul',this).stop(true, true).delay(200).slideDown(200);
	}, function() {
		jQuery(this).removeClass('current');
		jQuery('ul',this).stop(true, true).slideUp(200);
	});

	jQuery("#navHulp").hover(function(){
		jQuery(this).addClass('current');
		jQuery('ul',this).stop(true, true).delay(200).slideDown(200);
	}, function() {
		jQuery(this).removeClass('current');
		jQuery('ul',this).stop(true, true).slideUp(200);
	});

	jQuery("#navover").hover(function(){
		jQuery(this).addClass('current');
		jQuery('ul',this).stop(true, true).delay(200).slideDown(200);
	}, function() {
		jQuery(this).removeClass('current');
		jQuery('ul',this).stop(true, true).slideUp(200);
	});


	var pathname = window.location.pathname;
	pathname_a = pathname.split("/");
	var l = pathname_a.length;
	$('.megamenu li').each(function(){
		var at = $(this).find('a').attr('href');
		if (at == pathname) {
			$(this).addClass('active');
		}
	});
	var myParam = '/undefined';
	if($('#body_design').length >0) {
		myParam = '/' + location.search.split('from_page=')[1];
	}


	$("#mainmenu li").find('ul').parent().addClass('has-child');

	$("#mainmenu li h3").each(function(){
		var at = $(this).find('a').attr('href');
		if (at == pathname){
			$(this).addClass("open");
			$(this).next("ul").show();
		}
	});

	$("#mainmenu li ul li").each(function(){
		var at = $(this).find('a').attr('href');
		var at_a = at.split("/");
		/*if (at == pathname) {
			$(this).find('a').parent().addClass('active');
			$(this).parent().prev().addClass("open");
			$(this).parent().show();
		}*/
		if (myParam != '/undefined') {
			if(myParam === at) {
				$(this).find('a').parent().addClass('active');
				$(this).parent().prev().addClass("open");
				$(this).parent().show();
			}
		} else {

		if (pathname.search(at+'/') != -1) {
			$(this).find('a').parent().addClass('active');
			$(this).parent().prev().addClass("open");
			$(this).parent().show();
		}}
	});

	$("#mainmenu .has-child h3 a").click(function(e){
		e.preventDefault();
		var inner=$(this).parents("li").find("ul");
		if($(this).parent().hasClass("open")){
			$(this).parent().removeClass("open");
			inner.slideUp(300);
		}else{
			$("#mainmenu li ul").not(inner).hide();
			$("#mainmenu li h3").removeClass("open");
			$(this).parent().addClass("open");
			inner.slideDown(300);
		}
	});

	var loc = window.location.href;
	if(/geboortekaartjes/.test(loc)) {
		$('body').addClass('my-geboortekaartjes');
		$('.my-geboortekaartjes #mainmenu .geboo-item h3').addClass('open').next().show();
	}


        $('#rich-menu-content').appendTo($('#categories')); // set visual to visual content in header

    if($('li.Kleuren').length){
        $('body').append('<div class="cToolTip" />');
        $('li.Kleuren li.tag-li label').hover(
            function() {
                var kleurtje = $.trim($(this).text());
                kleurtje = kleurtje.split(" ");
                $('.cToolTip').html(kleurtje[0]).show();
            }, function() {
                $('.cToolTip').hide();
            }
        );

        $('li.Kleuren li.tag-li label').mousemove(function( e ) {
            $(".cToolTip").css({left:e.pageX+10,top:e.pageY-10});
        });
    }

	$("#mainmenu li ul li a[href='" + window.location.pathname + "']").addClass('active');
	$("#mainmenu li ul li a[href='" + window.location.pathname + "']").parent('li').parent('ul').show();

	if (window.location.pathname == '/geboortekaartjes-eigen-gezin-zelf-samenstellen'){
		$("#mainmenu li ul li a[href='/geboortekaartje-voor-eigen-gezin-zelf-samenstellen']").addClass('active');
		$("#mainmenu li ul li a[href='/geboortekaartje-voor-eigen-gezin-zelf-samenstellen']").parents('ul').prev().addClass('open');
		$("#mainmenu li ul li a[href='/geboortekaartje-voor-eigen-gezin-zelf-samenstellen']").parent('li').parent('ul').show();
	}
	if (window.location.pathname == '/geboortekaartjes-eigen-gezin-samenstellen-by-alma'){
		$("#mainmenu li ul li a[href='/geboortekaartje-voor-eigen-gezin-zelf-samenstellen']").addClass('active');
		$("#mainmenu li ul li a[href='/geboortekaartje-voor-eigen-gezin-zelf-samenstellen']").parents('ul').prev().addClass('open');
		$("#mainmenu li ul li a[href='/geboortekaartje-voor-eigen-gezin-zelf-samenstellen']").parent('li').parent('ul').show();
	}
	$("#body_design .description").append('<div><strong style="color:#C03321;">Proefdruk &euro;1,- &amp; gratis verzending.</strong></div>');

});